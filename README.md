#Fedora4, Sufia, docker recipe. 
Works. More or less. 

## Reference
https://github.com/curationexperts/hydradam/wiki/Production-Installation%3A-Overview

https://github.com/projecthydra/sufia

http://www.talkingquickly.co.uk/2014/06/rails-development-environment-with-vagrant-and-docker/

### Note: SELinux. 
  - `sudo getenforce` # to see if it is running
  - If SELinux is running and you are bind mounting (-v flag) a directory on the docker host to the docker container, you will need to either use the  "--privileged=true" flag on the docker run command or `chcon -RT` the dir's on the host that are being bind mounted.
  - `sudo chcon -Rt svirt_sandbox_file_t /large/volume/on/host`
  - `sudo chcon -Rt svirt_sandbox_file_t /pathonhost/to/railsappdir`

### Other notes
 - you must stop druw-development before running druw-production or the pids will clash.
 - httpd runs as apache -- get UID and chown -R your rails app.
 - set env variable for PROJDIR

## development/production summary

images

 - hydrabase
 - tomcat/fedora4solr
 - redis (using a docker stock image for now)
 - postgresql (not built yet)
 - sufia

containers-dev

 - redis (same container for dev and prod)
 - sufia (start with rails server)

containers-production

 - tomcat/f4solr-prod
 - redis (same container for dev and prod)
 - postgres (image not built yet, just use sqlite3)
 - sufia (start with apache/mod-passenger)

# Setup

## clone this repo

## build the following 3 docker images
   - druw/hydrabase image
      - `sudo docker build -t druw/hydrabase hydrabase/.`
   - tomcat (you have to be in tomcat/ dir to build this image)
      - `cd tomcat`
      -  `sudo docker build -t tomcat:f4solr .`  
        druw.xml contains location information for rails app -- change accordingly.  
        schema.xml, solrconfig.xml  are also from druw repo -- not sure when those change.
   - sufia (you have to be in sufia/ dir to build this image)
      - modify and rename druw.conf.template and passenger.conf.template before building
      - `cd sufia`
      - `sudo docker build -t druw .`

## run the following 3 containers
   - redis (No local repo for this yet so it is pulling a stock docker redis image)
     - `sudo docker run --name redis -d redis`  
        If you name the redis container something else, don't forget to change it in redis.yml.l  
   - tomcat-p (production container for fedora and solr)
     - `sudo docker run -d -p 127.0.0.1:8083:8080 --name tomcat-p tomcat:f4solr`
       or 
     - `sudo docker run -d -p 80:80 -v /large/volume/on/host:/usr/local/tomcat/fcrepo4-data --name tomcat-p tomcat:f4solr`
        - SELinux: chcon -Rt svirt_sandbox_file_t /large/volume/on/host
       Check if this is working by going to http://localhost:8082/fedora and http://localhost:8083/druw on host machine.

## clone druw repo or whatever sufia repo you want
   - Set your PROJDIR environment variable
   - `export PROJDIR=$PWD`

# Development

## Set up railsapp using docker run commands
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle install --path vendor/bundle"`

## Create secrets string for development
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake secret > /opt/druw/config/secrets.yml"`

## Edit secrets.yml for dev  
    development:  
        secret_key_base: $THATBIGSTRING  

(set indent at 2 spaces or whatever yml wants)

## Migrate development db
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake db:migrate"`

## Run this thing

`sudo docker run --privileged=true -d -p 127.0.0.1:3300:3000 --name rails -v $PROJDIR/druw:/opt/druw --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && rake jetty:start && /usr/local/bin/rails server -b 0.0.0.0"`

# Production
 * note: you must stop druw-development before running druw-production or the pids will clash.

## Set up railsapp using docker run commands
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle install --path vendor/bundle"`

## Create secrets string for production
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake secret >> /opt/druw/config/secrets.yml"`
 - Edit secrets.yml for prod

## Migrate production db
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake db:migrate"`

## Precompile assets for production
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake assets:precompile"`

## chown druw/ dir to apache's uid
`sudo chown -R 48 $PROJDIR/druw` or whatever the uid is.

## Run this thing

`sudo docker run --privileged=true -d -p 127.0.0.1:3400:80 --name rails -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment production start && /usr/sbin/httpd -D FOREGROUND"`

or

`sudo docker run -d -p 80:80 --name rails -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment production start && /usr/sbin/httpd -D FOREGROUND"`  
   - SELinux `sudo chcon -Rt /path/to/railsappdir:/pathoncontainer/to/railsappdir`

# Scripts
the bash scripts in scripts/ dir work more or less and they are just here for convenience.
$PROJDIR needs to be set or these scripts won't work.
                                                                                                   
`sh $PROJDIR/scripts/stop_rails.sh` stops rails server and resque workers, then gets rid of PIDs.  
- careful with the stop script, it just will stop whatever the last running container was and will get rid of any container that is not running.

`sh $PROJDIR/scripts/rebuild_app.sh` tomcat and redis need to still be running.
 
`sh $PROJDIR/scripts/start_rails.sh` starts resque workers and rails server
                                                                                                       
`sh $PROJDIR/scripts/prebuild_app.sh` tomcat-p and redis need to still be running.

`sh $PROJDIR/scripts/pstart_rails.sh` starts resque workers and rails server

# Docker cheat sheet
`sudo docker images` shows local docker images (available on the host)

`sudo docker ps` shows running containers

`sudo docker ps -a` shows all running and stopped containers

`sudo docker ps -l` shows the last container started

`sudo docker exec -it [containerid or containername] bash` gets you into a running container

`sudo docker run -it [containerid or containername] bash` runs a container from an image and gets you into a bash shell there.

`sudo docker rm $(sudo docker ps -a -q)` deletes all stopped containers

`sudo docker rmi [imageid or imagename]` deletes that image

`sudo docker save [imageid or imagename] > /filepath/filename.tar.gz` saves an image so you can move it to another host

`sudo docker load < filename.tar.gz` loads a saved docker image

`sudo docker export [containerid or containername] | sudo docker import - [newimagename]:[tag]`
 # http://tuhrig.de/flatten-a-docker-container-or-image/