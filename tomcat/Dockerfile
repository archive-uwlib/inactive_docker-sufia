FROM centos:7

ENV JAVA_VERSION 1.8.0
ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.0.29
ENV TOMCAT_TGZ_URL https://www.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
ENV SOLR_VERSION 4.10.4
ENV SOLR solr-$SOLR_VERSION
ENV FEDORA_VERSION 4.2.0
ENV HYDRA_NAME druw

RUN yum -y update && yum -y install tar wget java-"${JAVA_VERSION}"-openjdk && yum clean all

RUN mkdir -p "$CATALINA_HOME" 
WORKDIR $CATALINA_HOME

RUN curl -SL "$TOMCAT_TGZ_URL" -o tomcat.tar.gz \ 
    && curl -SL "$TOMCAT_TGZ_URL.asc" -o tomcat.tar.gz.asc \
    && tar -xvf tomcat.tar.gz --strip-components=1 \
    && rm bin/*.bat \
    && rm tomcat.tar.gz*

# Install solr      

RUN mkdir -p /opt/install
RUN cd /opt/install && wget http://archive.apache.org/dist/lucene/solr/$SOLR_VERSION/$SOLR.tgz 

#RUN tar -C /opt --extract --file /opt/$SOLR.tgz
RUN cd /opt/install/ && tar zxf /opt/install/$SOLR.tgz && rm /opt/install/$SOLR.tgz
RUN mkdir -p /opt/solr/$HYDRA_NAME/lib
RUN cp /opt/install/$SOLR/dist/$SOLR.war /opt/solr/$HYDRA_NAME
RUN cp /opt/install/$SOLR/dist/*.jar /opt/solr/$HYDRA_NAME/lib
RUN cp -r /opt/install/$SOLR/contrib /opt/solr/$HYDRA_NAME/lib
RUN cp -r /opt/install/$SOLR/example/solr/collection1 /opt/solr/$HYDRA_NAME
RUN cp -r /opt/install/$SOLR/example/solr/collection1/conf/lang/stopwords_en.txt /opt/solr/$HYDRA_NAME/collection1/conf
ADD ./$HYDRA_NAME.xml /opt/solr/$HYDRA_NAME/$HYDRA_NAME.xml
RUN mkdir -p /usr/local/tomcat/conf/Catalina/localhost
RUN ln -s /opt/solr/$HYDRA_NAME/$HYDRA_NAME.xml /usr/local/tomcat/conf/Catalina/localhost/$HYDRA_NAME.xml
RUN cp /opt/install/$SOLR/example/lib/ext/*.jar /usr/local/tomcat/lib/
ADD ./schema.xml /opt/solr/$HYDRA_NAME/collection1/conf/schema.xml
ADD ./solrconfig.xml /opt/solr/$HYDRA_NAME/collection1/conf/solrconfig.xml

# Install fedora4
RUN cd /usr/local/tomcat/webapps && wget http://repo1.maven.org/maven2/org/fcrepo/fcrepo-webapp/$FEDORA_VERSION/fcrepo-webapp-$FEDORA_VERSION.war && mv fcrepo-webapp-$FEDORA_VERSION.war /usr/local/tomcat/webapps/fedora.war

RUN groupadd -r tomcat && \
  useradd -g tomcat -d ${CATALINA_HOME} -s /sbin/nologin -c "Tomcat user" tomcat && \
  chown -R tomcat:tomcat ${CATALINA_HOME} && \
  chown -R tomcat:tomcat /opt/solr

EXPOSE 8080
USER tomcat
                                                                                             
CMD ["catalina.sh", "run"]    
