#! /usr/bin/env

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake assets:clean"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake assets:clobber"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle install --path vendor/bundle"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake assets:precompile"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && rails generate sufia:models:update_content_blocks"

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && rails generate sufia:upgrade600"

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=production" --link tomcat-p:tomcat-p --link redis:redis --entrypoint /bin/bash --rm druw:latest -c "cd /opt/druw && bundle exec rake db:migrate"
