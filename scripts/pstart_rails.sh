#! /usr/bin/env bash

#sudo docker run --privileged=true -d -p 127.0.0.1:3400:80 -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment production start && /usr/sbin/httpd -D FOREGROUND"

sudo docker run -d -p 127.0.0.1:3400:80 --name rails -v $PROJDIR/druw:/opt/druw --link tomcat-p:tomcat-p --link redis:redis -e "RAILS_ENV=production" --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment production start && /usr/sbin/httpd -D FOREGROUND"

