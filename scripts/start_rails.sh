#! /usr/bin/env bash

sudo docker run --privileged=true -d -p 127.0.0.1:3300:3000 --name rails -v $PROJDIR/druw:/opt/druw --link redis:redis druw:latest bash -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && rake jetty:start && /usr/local/bin/rails server -b 0.0.0.0"

#sudo docker run -d -p 127.0.0.1:3300:3000 --name rails -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && /usr/local/bin/rails server -b 0.0.0.0"
