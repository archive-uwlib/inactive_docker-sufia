#! /usr/bin/env

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec rake assets:clean"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec rake assets:clobber"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle install --path vendor/bundle"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && rake jetty:clean"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && rake jetty:config"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw -e "RAILS_ENV=development" --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec rake db:migrate"
